from lxml import etree
doc=etree.parse("senderos.xml")
from funcionessenderos import *
while True:
    print('''
    1. LISTADO DE MUNICIPIOS
    2. MUESTRA LOS MUNICIPIOS E INDICA CUANTOS SENDEROS TIENE
    3. INTRODUCE SENDERO Y MUESTRA SI NECESITA PERMISO, DURACION Y DIFICULTAD DE LA RUTA
    4. MUESTRA LAS COORDENADAS DEL SENDERO EN OpenStreetMap
    0. Salir
    ''')
    opcion=int(input("Introduce una opcion: "))

    if opcion==1:
        for municipio in mostrar_municipios(doc):
            print(municipio) 
    if opcion==2:
       for dato in contar_senderos(doc):
           print("%s tiene %d senderos"%(dato[0],dato[1]))
    if opcion==3:
        nombre_sendero=input("Introduce el Nombre de un Sendero: ")
        informacion=info_sendero(doc,nombre_sendero)
        print('''
        Nombre: %s
        Permiso: %s
        Duracion: %s
        Dificultad: %s
        Info.: %s'''%(nombre_sendero,informacion[0],informacion[1],informacion[2],informacion[3]))
    if opcion==4:
        nombre_sendero=input("Introduce el Nombre de un Sendero: ")
        localizacion=localizacion(doc,nombre_sendero)
        print("https://www.openstreetmap.org/#map=14/%s/%s"%(localizacion[1],localizacion[0]))
    if opcion==0:
        break