# ejercicio_xml

El primer ejercicio **listar informacion** voy a listar los municipios que tienen senderos del fichero xml, en el ejercicio **Contar información** voy a mostrar la cantidad de rutas de senderismo que tiene cada municipio, en el ejercicio **Buscar o filtrar información** introduciremos el nombre de un sendero y nos indicara si necesita permiso para poder realizar la ruta, la duración de la ruta y la dificultad de la ruta en **El ejercicio libre** mostraremos la localización de la ruta en OpenStreetMap.

