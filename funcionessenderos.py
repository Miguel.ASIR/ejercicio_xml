#Muestra el nombre de los municipios ordenados alfabeticamente
def mostrar_municipios(doc):
    municipios=[]
    for municipio in doc.xpath('//resource/str[@name="ca:municipio"]/text()'):
        if municipio not in municipios:
            municipios.append(municipio)
    return sorted(municipios)  
#Contar el numero de senderos por municipio
def contar_senderos(doc):
    municipios=doc.xpath('//resource/str[@name="ca:municipio"]/text()')
    cantidad=[]
    send_prov=[]
    for nom_municipio in municipios:
        datos=[]
        datos.append(nom_municipio)
        datos.append(municipios.count(nom_municipio))
        cantidad.append(datos)
    for cont in range(len(cantidad)):
        if cantidad[cont] not in send_prov:
            send_prov.append(cantidad[cont])
    return send_prov
#Segun el sendero indica si hace falta descripcion,permiso,duracion y dificultad de la ruta
def info_sendero(doc,nombre_sendero):
    if nombre_sendero in doc.xpath('//resource/str[@name="ca:nombre"]/text()'):
        datos=[]
        indice=doc.xpath('//resource/str[@name="ca:nombre"]/text()').index(nombre_sendero)
        datos.append(doc.xpath('//resource/str[@name="ca:permiso"]/text()')[indice])
        datos.append(doc.xpath('//resource/str[@name="ca:duracion"]/text()')[indice])
        datos.append(doc.xpath('//resource/str[@name="ca:dificultad"]/text()')[indice])
        datos.append(doc.xpath('//resource/str[@name="ca:proteccion"]/text()')[indice])
        return datos
    else:
        return "El Sendero introducido no Existe."
#Segun el nombre del sendero nos indica la localización en OpenStreetMap
def localizacion(doc,nombre_sendero):
     if nombre_sendero in doc.xpath('//resource/str[@name="ca:nombre"]/text()'):
        datos=[]
        indice=doc.xpath('//resource/str[@name="ca:nombre"]/text()').index(nombre_sendero)
        datos.append(doc.xpath('//resource/str[@name="ca:coord_longitud"]/text()')[indice])
        datos.append(doc.xpath('//resource/str[@name="ca:coord_latitud"]/text()')[indice])
        return datos